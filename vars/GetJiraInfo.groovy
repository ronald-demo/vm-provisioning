@NonCPS
def getJiraIssueDetail() {
    /**
     * Get parameters from Jira ticket
     * This requires the Jenkins JIRA Pipeline Steps plugin https://jenkinsci.github.io/jira-steps-plugin/getting-started/
     * @param None
     */ 
    def ticket = jiraGetIssue  idOrKey: "$IssueNo", site: 'jira'
    def summary = ticket.data.fields.summary.toString()
    def bu = ticket.data.fields.customfield_10061.value.toString()
    def env = ticket.data.fields.customfield_10060.value.toString()
    def cc = ticket.data.fields.customfield_10059.toString()
    def instance = ticket.data.fields.customfield_10057.value.toString()
    def os = ticket.data.fields.customfield_10058.value.toString()
} return this;