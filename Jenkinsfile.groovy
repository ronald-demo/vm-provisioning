def ticket = jiraGetIssue  idOrKey: "$IssueNo", site: 'jira'
def summary = ticket.data.fields.summary.toString()
def bu = ticket.data.fields.customfield_10061.value.toString()
def env = ticket.data.fields.customfield_10060.value.toString()
def cc = ticket.data.fields.customfield_10059.toString()
def instance = ticket.data.fields.customfield_10057.value.toString()
def os = ticket.data.fields.customfield_10058.value.toString()

pipeline {
    agent any      
    stages {
        stage('Jira Ticket Info') {
            steps {
                println """
                Summary: $summary
                BU: $bu
                CC: $cc
                Env: $env
                Instance: $instance
                Operating Systems: $os
                """
            }
        }
        stage ('VM Provisioning') {
            steps {
            echo 'Provisioning'
            sh """
            #!/bin/bash
            gcloud compute instances create instance-1 --zone=asia-southeast2-a --machine-type=$instance --subnet=default --image-family=centos-7 --image-project=centos-cloud
            """
            }
        }
        stage ('Confluence Update') {
            steps {
            echo 'Uploading information to confluence'
            /*
            withCredentials([usernameColonPassword(credentialsId: 'jira', variable: 'confluence_creds')]) {
            sh '''
            #!/bin/bash
            TEXT= ${FILE,path="/html/template.html"}
            echo '{"type":"page","title":"'$ConfluencePageName'","space":{"key":"DEVSECOPS"},"body":{"storage":{"value":"'$TEXT'","representation":"storage"}}}' > update.json
            curl -u ${confluence_creds} -X POST -H 'Content-Type: application/json' -d '@update.json' https://sandy-bdi.atlassian.net/wiki/rest/api/content/ | python -mjson.tool
            rm update.json
            '''
            }*/
            }
        }
        stage ('Update Jira Status') {
            steps {
            echo 'updating jira status'
            withEnv(['JIRA_SITE=Jira']) {
                    script {
                    def transitionInput = [transition: [id: '141']]
                    jiraTransitionIssue idOrKey: "$IssueNo", input: transitionInput
                    }
                }
            }
        }
    }
    post {
    always {
        deleteDir() /* clean up our workspace */
    }
    success {
        echo 'I succeeded!'
    }
    unstable {
        echo 'I am unstable :/'
    }
    failure {
        echo 'I failed :('
    }
    changed {
        echo 'Things were different before...'
    }
}
}

